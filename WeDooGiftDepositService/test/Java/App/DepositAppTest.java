package Java.App;

import Java.Controller.DefaultCompanyController;
import Java.Controller.DefaultCustomerController;
import Java.Model.BusinessElements.Company.DefaultCompany;
import Java.Model.BusinessElements.Customer.DefaultCustomer;
import Java.Model.BusinessElements.Deposits.DepositFactory;
import Java.Model.BusinessElements.Deposits.DepositTypes;
import Java.Model.BusinessElements.ExceptionHandeling.BalanceError;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DepositAppTest {

    @Test
    void issueGiftForCustomer() throws BalanceError {
        var companyController = new DefaultCompanyController();
        var customerController = new DefaultCustomerController();
        var depositFactory = new DepositFactory();


        var depositApp = new DepositApp(companyController, customerController, depositFactory);
        DefaultCompany testCompany = new DefaultCompany(1,"Test Company");
        testCompany.setBalance(500);

        var customer = new DefaultCustomer(1,"Jack");

        depositApp.getAppCompany().add(testCompany);
        depositApp.getAppCustomer().add(customer);

        depositApp.issueGiftForCustomer(1, depositApp.getAppCompany().get(0),depositApp.getAppCustomer().get(0), 100, DepositTypes.GIFT_DEPOSIT);
        assertFalse(depositApp.getAppCustomer().get(0).getGiftDistributions().isEmpty());
    }

    @Test
    void addGiftToHistoryWithNoCustomerHistory() throws BalanceError {
        var companyController = new DefaultCompanyController();
        var customerController = new DefaultCustomerController();
        var depositFactory = new DepositFactory();


        var depositApp = new DepositApp(companyController, customerController, depositFactory);
        DefaultCompany testCompany = new DefaultCompany(1,"Test Company");
        testCompany.setBalance(500);

        var customer = new DefaultCustomer(1,"Jack");

        depositApp.getAppCompany().add(testCompany);
        depositApp.getAppCustomer().add(customer);

        assertTrue(depositApp.getAppCompany().get(0).getGiftDistributions().isEmpty());

        depositApp.issueGiftForCustomer(1, depositApp.getAppCompany().get(0),depositApp.getAppCustomer().get(0), 100, DepositTypes.GIFT_DEPOSIT);
        assertFalse(depositApp.getAppCompany().get(0).getGiftDistributions().isEmpty());

    }

    @Test
    void addGiftToHistoryWithCustomerHistory() throws BalanceError {
        var companyController = new DefaultCompanyController();
        var customerController = new DefaultCustomerController();
        var depositFactory = new DepositFactory();


        var depositApp = new DepositApp(companyController, customerController, depositFactory);
        DefaultCompany testCompany = new DefaultCompany(1,"Test Company");
        testCompany.setBalance(500);

        var customer = new DefaultCustomer(1,"Jack");

        depositApp.getAppCompany().add(testCompany);
        depositApp.getAppCustomer().add(customer);

        assertTrue(depositApp.getAppCompany().get(0).getGiftDistributions().isEmpty());

        depositApp.issueGiftForCustomer(1, depositApp.getAppCompany().get(0),depositApp.getAppCustomer().get(0), 100, DepositTypes.GIFT_DEPOSIT);
        assertEquals(1,depositApp.getAppCompany().get(0).getGiftDistributions().keySet().size());
        assertEquals(1,depositApp.getAppCompany().get(0).getGiftDistributions().get(customer.getId()).size());

        depositApp.issueGiftForCustomer(2, depositApp.getAppCompany().get(0),depositApp.getAppCustomer().get(0), 200, DepositTypes.MEAL_DEPOSIT);
        assertEquals(1,depositApp.getAppCompany().get(0).getGiftDistributions().keySet().size());
        assertEquals(2,depositApp.getAppCompany().get(0).getGiftDistributions().get(customer.getId()).size());

    }

    @Test
    void chargeCompanyBalance(){
        var companyController = new DefaultCompanyController();
        var customerController = new DefaultCustomerController();
        var depositFactory = new DepositFactory();


        var depositApp = new DepositApp(companyController, customerController, depositFactory);
        DefaultCompany testCompany = new DefaultCompany(1,"Test Company");
        depositApp.getAppCompany().add(testCompany);

        depositApp.chargeCompanyBalance(depositApp.getAppCompany().get(0).getId(),800.0);

        assertEquals(800.0,depositApp.getAppCompany().get(0).getBalance());
    }

    @Test
    void UseGift() throws BalanceError {
        var companyController = new DefaultCompanyController();
        var customerController = new DefaultCustomerController();
        var depositFactory = new DepositFactory();


        var depositApp = new DepositApp(companyController, customerController, depositFactory);
        DefaultCompany testCompany = new DefaultCompany(1,"Test Company");
        testCompany.setBalance(500);

        var customer = new DefaultCustomer(1,"Jack");

        depositApp.getAppCompany().add(testCompany);
        depositApp.getAppCustomer().add(customer);

        depositApp.issueGiftForCustomer(1, depositApp.getAppCompany().get(0),depositApp.getAppCustomer().get(0), 100, DepositTypes.GIFT_DEPOSIT);

        depositApp.useGift(1, depositApp.getAppCustomer().get(0).getGiftDistributions().get(0).getId());

        assertEquals(0,depositApp.getAppCustomer().get(0).getGiftDistributions().size());
    }



}