package Java.Controller;

import Java.App.DepositApp;
import Java.Model.BusinessElements.Company.DefaultCompany;
import Java.Model.BusinessElements.Customer.DefaultCustomer;
import Java.Model.BusinessElements.Deposits.DepositFactory;
import Java.Model.BusinessElements.Deposits.DepositTypes;
import Java.Model.BusinessElements.ExceptionHandeling.BalanceError;
import Java.Model.BusinessElements.ExceptionHandeling.GiftError;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class DefaultCustomerControllerTest {

    @Test
    void calculateBalanceWithValidGifts() throws BalanceError, GiftError {
        var companyController = new DefaultCompanyController();
        var customerController = new DefaultCustomerController();
        var depositFactory = new DepositFactory();


        var depositApp = new DepositApp(companyController, customerController, depositFactory);
        DefaultCompany testCompany = new DefaultCompany(1,"Test Company");
        testCompany.setBalance(500);

        var customer = new DefaultCustomer(1,"Jack");

        depositApp.getAppCompany().add(testCompany);
        depositApp.getAppCustomer().add(customer);

        depositApp.issueGiftForCustomer(1, depositApp.getAppCompany().get(0),depositApp.getAppCustomer().get(0), 100, DepositTypes.GIFT_DEPOSIT);
        assertFalse(depositApp.getAppCustomer().get(0).getGiftDistributions().isEmpty());

        depositApp.getDefaultCustomerController().calculateBalance(depositApp.getAppCustomer().get(0));
        double balance = depositApp.getAppCustomer().get(0).getBalance();

        assertEquals(100,balance);
    }

    @Test
    void calculateBalanceWithValidAnValidGiftDeposit() throws BalanceError, GiftError {
        var companyController = new DefaultCompanyController();
        var customerController = new DefaultCustomerController();
        var depositFactory = new DepositFactory();


        var depositApp = new DepositApp(companyController, customerController, depositFactory);
        DefaultCompany testCompany = new DefaultCompany(1,"Test Company");
        testCompany.setBalance(500);

        var customer = new DefaultCustomer(1,"Jack");

        depositApp.getAppCompany().add(testCompany);
        depositApp.getAppCustomer().add(customer);

        depositApp.issueGiftForCustomer(1, depositApp.getAppCompany().get(0),depositApp.getAppCustomer().get(0), 100, DepositTypes.GIFT_DEPOSIT);

        final Calendar c = Calendar.getInstance();
        c.setTime(depositApp.getAppCustomer().get(0).getGiftDistributions().get(0).getIssueDate());
        c.add(Calendar.DATE, -367);
        var expiredDate =  c.getTime();

        depositApp.getAppCustomer().get(0).getGiftDistributions().get(0).setIssueDate(expiredDate);
        depositApp.getDefaultCustomerController().calculateBalance(depositApp.getAppCustomer().get(0));

        double balance = depositApp.getAppCustomer().get(0).getBalance();

        assertEquals(0.0,balance);
    }

    @Test
    void calculateBalanceWithValidAnValidMealDeposit() throws BalanceError, GiftError {
        var companyController = new DefaultCompanyController();
        var customerController = new DefaultCustomerController();
        var depositFactory = new DepositFactory();


        var depositApp = new DepositApp(companyController, customerController, depositFactory);
        DefaultCompany testCompany = new DefaultCompany(1,"Test Company");
        testCompany.setBalance(500);

        var customer = new DefaultCustomer(1,"Jack");

        depositApp.getAppCompany().add(testCompany);
        depositApp.getAppCustomer().add(customer);

        depositApp.issueGiftForCustomer(1, depositApp.getAppCompany().get(0),depositApp.getAppCustomer().get(0), 100, DepositTypes.MEAL_DEPOSIT);

        depositApp.getDefaultCustomerController().calculateBalance(depositApp.getAppCustomer().get(0));

        double balance = depositApp.getAppCustomer().get(0).getBalance();

        assertEquals(100.0,balance);
    }

    @Test
    void calculateBalanceWithValidAnInvalidMealDeposit() throws BalanceError, GiftError {
        var companyController = new DefaultCompanyController();
        var customerController = new DefaultCustomerController();
        var depositFactory = new DepositFactory();


        var depositApp = new DepositApp(companyController, customerController, depositFactory);
        DefaultCompany testCompany = new DefaultCompany(1,"Test Company");
        testCompany.setBalance(500);

        var customer = new DefaultCustomer(1,"Jack");

        depositApp.getAppCompany().add(testCompany);
        depositApp.getAppCustomer().add(customer);

        depositApp.issueGiftForCustomer(1, depositApp.getAppCompany().get(0),depositApp.getAppCustomer().get(0), 100, DepositTypes.MEAL_DEPOSIT);

        final Calendar c = Calendar.getInstance();
        c.setTime(depositApp.getAppCustomer().get(0).getGiftDistributions().get(0).getIssueDate());
        c.add(Calendar.DATE, -367);
        var expiredDate =  c.getTime();

        depositApp.getAppCustomer().get(0).getGiftDistributions().get(0).setIssueDate(expiredDate);
        depositApp.getDefaultCustomerController().calculateBalance(depositApp.getAppCustomer().get(0));

        double balance = depositApp.getAppCustomer().get(0).getBalance();

        assertEquals(0.0,balance);
    }
}