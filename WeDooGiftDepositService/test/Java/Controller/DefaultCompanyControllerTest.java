package Java.Controller;

import Java.App.DepositApp;
import Java.Model.BusinessElements.Company.DefaultCompany;
import Java.Model.BusinessElements.Customer.DefaultCustomer;
import Java.Model.BusinessElements.Deposits.DefaultDeposit;
import Java.Model.BusinessElements.Deposits.DepositFactory;
import Java.Model.BusinessElements.Deposits.DepositTypes;
import Java.Model.BusinessElements.Deposits.impl.DefaultGiftDeposit;
import Java.Model.BusinessElements.ExceptionHandeling.BalanceError;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class DefaultCompanyControllerTest {

    @Test
    void isBalanceGreaterThanPaymentWhenBalanceIsOver() {
        var companyController = new DefaultCompanyController();
        var customerController = new DefaultCustomerController();
        var depositFactory = new DepositFactory();


        var depositApp = new DepositApp(companyController, customerController, depositFactory);


        DefaultCompany testCompany = new DefaultCompany(1,"Test Company");
        testCompany.setBalance(500);

        depositApp.getAppCompany().add(testCompany);

        boolean test =  depositApp.getDefaultCompanyController().isBalanceGreaterThanPayment(depositApp.getAppCompany().get(0),400);

        assertTrue(test);
    }

    @Test
    void isBalanceGreaterThanPaymentWhenBalanceIsInsufisant() {
        var companyController = new DefaultCompanyController();
        var customerController = new DefaultCustomerController();
        var depositFactory = new DepositFactory();


        var depositApp = new DepositApp(companyController, customerController, depositFactory);


        DefaultCompany testCompany = new DefaultCompany(1,"Test Company");
        testCompany.setBalance(500);

        depositApp.getAppCompany().add(testCompany);

        boolean test =  depositApp.getDefaultCompanyController().isBalanceGreaterThanPayment(depositApp.getAppCompany().get(0),800);

        assertFalse(test);
    }

}