package Java.Controller;

import Java.Model.BusinessElements.Company.DefaultCompany;
import Java.Model.BusinessElements.Customer.DefaultCustomer;
import Java.Model.BusinessElements.Deposits.DefaultDeposit;

import java.util.ArrayList;
import java.util.List;

public class DefaultCompanyController {

    public boolean isBalanceGreaterThanPayment(DefaultCompany company, double amount){
        return company.getBalance() - amount > 0;
    }
}
