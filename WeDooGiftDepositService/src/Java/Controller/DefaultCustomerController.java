package Java.Controller;

import Java.Model.BusinessElements.Customer.DefaultCustomer;
import Java.Model.BusinessElements.Deposits.DefaultDeposit;
import Java.Model.BusinessElements.Deposits.IDeposit;
import Java.Model.BusinessElements.ExceptionHandeling.GiftError;

import java.util.Objects;

public class DefaultCustomerController {

    public void calculateBalance(DefaultCustomer customer) throws GiftError {
        if(!customer.getGiftDistributions().isEmpty()) {

            customer.getGiftDistributions().forEach(IDeposit::updateExpirationStatus);

            final double balance = customer.getGiftDistributions().stream()
                    .filter(gift -> Boolean.FALSE.equals(gift.isExpired()))
                    .mapToDouble(DefaultDeposit::getAmount).sum();
            customer.setBalance(balance);
        } else{
            throw new GiftError("You have no gift linked to you account");
        }
    }

}
