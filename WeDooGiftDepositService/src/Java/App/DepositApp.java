package Java.App;

import Java.Controller.DefaultCompanyController;
import Java.Controller.DefaultCustomerController;
import Java.Model.BusinessElements.Company.DefaultCompany;
import Java.Model.BusinessElements.Customer.DefaultCustomer;
import Java.Model.BusinessElements.Deposits.DefaultDeposit;
import Java.Model.BusinessElements.Deposits.DepositFactory;
import Java.Model.BusinessElements.Deposits.DepositTypes;
import Java.Model.BusinessElements.ExceptionHandeling.BalanceError;
import Java.Model.BusinessElements.ExceptionHandeling.GiftError;
import Java.Model.BusinessElements.ExceptionHandeling.WrongInputDepositTypeError;

import java.util.*;

public class DepositApp {

    private DefaultCompanyController defaultCompanyController;
    private DefaultCustomerController defaultCustomerController;
    private DepositFactory depositFactory;

    private List<DefaultCustomer> appCustomer;
    private List<DefaultCompany> appCompany;

    public DepositApp(DefaultCompanyController defaultCompanyController, DefaultCustomerController defaultCustomerController, DepositFactory depositFactory){
        this.defaultCompanyController = defaultCompanyController;
        this.defaultCustomerController = defaultCustomerController;
        this.depositFactory = new DepositFactory();

        this.appCustomer = new ArrayList<>();
        this.appCompany = new ArrayList<>();
    }

    //Use a gift from a Customer
    public void useGift(double customerId, double depositID){

        Optional<DefaultCustomer> targetCustomer = appCustomer.stream().filter(customer -> customerId == customer.getId()).findFirst();
        Optional<DefaultDeposit> targetDeposit;

       //Update Status of Gifts then remove from Customer Gifts
        try {
            defaultCustomerController.calculateBalance(targetCustomer.get());
            targetDeposit = targetCustomer.get().getGiftDistributions().stream()
                                                                    .filter(gift -> depositID == gift.getId())
                                                                    .findFirst();
            if(targetDeposit.isPresent() && !targetDeposit.get().isExpired()){
                targetCustomer.get().getGiftDistributions().remove(targetDeposit.get());
                System.out.println("Gift Code Used");
            }
        } catch (GiftError giftError) {
            giftError.printStackTrace();
        }
    }

    //Charge the balance of the company to issue Gift for Customer
    public void chargeCompanyBalance(double id, Double moneyTransfer){
        DefaultCompany targetCompany =  this.getAppCompany().stream().filter(company -> id == company.getId()).findFirst().get();
        targetCompany.setBalance(targetCompany.getBalance() + moneyTransfer);

    }

    //Create a Gift for the Customer and add it to the history of the Company
    public void issueGiftForCustomer(double id,DefaultCompany company, DefaultCustomer customer, double couponValue, DepositTypes depositType) throws BalanceError {
        if(this.defaultCompanyController.isBalanceGreaterThanPayment(company,couponValue)){

            try {
                DefaultDeposit deposit = this.depositFactory.createDeposit(id,company.getName(),couponValue,depositType);
                company.setBalance(company.getBalance() - couponValue);
                customer.getGiftDistributions().add(deposit);
                if(company.getGiftDistributions().containsKey(customer.getId())){
                    company.getGiftDistributions().get(customer.getId()).add(deposit);
                }else{
                    List<DefaultDeposit> customerDeposits = new ArrayList<>();
                    customerDeposits.add(deposit);
                    company.getGiftDistributions().put(customer.getId(),customerDeposits);
                }

            } catch (WrongInputDepositTypeError wrongInputDepositTypeError) {
                wrongInputDepositTypeError.printStackTrace();
            }

        } else {
            throw new BalanceError("Insuffisant Balance");
        }
    }


    public DefaultCompanyController getDefaultCompanyController() {
        return defaultCompanyController;
    }

    public DefaultCustomerController getDefaultCustomerController() {
        return defaultCustomerController;
    }

    public DepositFactory getDepositFactory() {
        return depositFactory;
    }

    public List<DefaultCustomer> getAppCustomer() {
        return appCustomer;
    }

    public void setAppCustomer(List<DefaultCustomer> appCustomer) {
        this.appCustomer = appCustomer;
    }

    public List<DefaultCompany> getAppCompany() {
        return appCompany;
    }

    public void setAppCompany(List<DefaultCompany> appCompany) {
        this.appCompany = appCompany;
    }
}
