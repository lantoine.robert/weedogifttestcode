package Java.Model.BusinessElements.Deposits;

import java.util.Calendar;
import java.util.Date;

public abstract class DefaultDeposit implements IDeposit {
    private double id;
    private Date issueDate;
    private double amount;
    private String companyName;
    private DepositTypes depositTypes;
    private boolean isExpired;

    public DefaultDeposit(double id, String companyName, double amount,DepositTypes depositTypes){
        this.id = id;
        this.companyName = companyName;
        this.amount = amount;
        this.depositTypes = depositTypes;
        this.isExpired = false;
        this.issueDate = Calendar.getInstance().getTime();
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    private void buildGiftExpirationDate(){

    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public DepositTypes getDepositTypes() {
        return depositTypes;
    }

    public void setDepositTypes(DepositTypes depositTypes) {
        this.depositTypes = depositTypes;
    }

    public boolean isExpired() {
        return isExpired;
    }

    public void setExpired(boolean valid) {
        isExpired = valid;
    }
}
