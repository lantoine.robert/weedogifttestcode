package Java.Model.BusinessElements.Deposits.impl;

import Java.Model.BusinessElements.Deposits.DefaultDeposit;
import Java.Model.BusinessElements.Deposits.DepositTypes;
import Java.Model.BusinessElements.Deposits.IDeposit;

import java.util.Calendar;
import java.util.Date;

public class DefaultMealDeposit extends DefaultDeposit  implements IDeposit {

    private final int ADDED_VALIDITY_YEAR = 1;
    private final int END_VALIDITY_MONTH = Calendar.FEBRUARY;
    private final int END_VALIDITY_DAY = 1;

    public DefaultMealDeposit(double id, String companyName, double amount, DepositTypes depositTypes) {
        super(id, companyName, amount, depositTypes);
    }

    @Override
    public void updateExpirationStatus() {
        final Date expirationDate =  buildMealExpirationDate();
        final Date actualDate = Calendar.getInstance().getTime();

        final int result = actualDate.compareTo(expirationDate);

        this.setExpired(result >= 0);
    }

    private Date  buildMealExpirationDate(){

        final Calendar c = Calendar.getInstance();
        c.setTime(this.getIssueDate());
        c.add(Calendar.YEAR, ADDED_VALIDITY_YEAR);
        c.set(Calendar.DATE,END_VALIDITY_DAY);
        c.set(Calendar.MONTH, END_VALIDITY_MONTH);

        return c.getTime();
    }
}
