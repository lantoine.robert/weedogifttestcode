package Java.Model.BusinessElements.Deposits.impl;

import Java.Model.BusinessElements.Deposits.DefaultDeposit;
import Java.Model.BusinessElements.Deposits.DepositTypes;
import Java.Model.BusinessElements.Deposits.IDeposit;

import java.util.Calendar;
import java.util.Date;

public class DefaultGiftDeposit extends DefaultDeposit implements IDeposit {

    public DefaultGiftDeposit(double id, String companyName, double amount, DepositTypes depositTypes) {
        super(id, companyName, amount, depositTypes);
    }

    @Override
    public void updateExpirationStatus() {
        final Date expirationDate = buildGiftExpirationDate();
        final Date actualDate = Calendar.getInstance().getTime();

        final int result = actualDate.compareTo(expirationDate);

        this.setExpired(result >= 0);
    }


    private Date buildGiftExpirationDate(){

        final Calendar c = Calendar.getInstance();
        c.setTime(this.getIssueDate());
        c.add(Calendar.DATE, 366);

        return c.getTime();
    }
}
