package Java.Model.BusinessElements.Deposits;

public enum DepositTypes {
    GIFT_DEPOSIT,
    MEAL_DEPOSIT
}
