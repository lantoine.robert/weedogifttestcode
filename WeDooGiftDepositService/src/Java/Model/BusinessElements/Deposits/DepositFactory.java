package Java.Model.BusinessElements.Deposits;

import Java.Model.BusinessElements.Deposits.impl.DefaultGiftDeposit;
import Java.Model.BusinessElements.Deposits.impl.DefaultMealDeposit;
import Java.Model.BusinessElements.ExceptionHandeling.WrongInputDepositTypeError;

public class DepositFactory {

    public DepositFactory(){}

    public DefaultDeposit createDeposit(double id, String companyName, double amount, DepositTypes depositTypes) throws WrongInputDepositTypeError {
        switch (depositTypes){
            case GIFT_DEPOSIT : return new DefaultGiftDeposit(id, companyName,amount,depositTypes);
            case MEAL_DEPOSIT : return new DefaultMealDeposit(id, companyName,amount,depositTypes);
        }

        throw new WrongInputDepositTypeError("Saisie erronee : Ce type de depot n'existe pas");
    }
}
