package Java.Model.BusinessElements.Company;

import Java.Model.BusinessElements.Customer.DefaultCustomer;
import Java.Model.BusinessElements.Deposits.DefaultDeposit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultCompany {
    private double id;
    private String name;
    private double balance;
    private Map<Double,List<DefaultDeposit>> giftDistributions;

    //Constructors
    public DefaultCompany(double id,String name){
        this.id = id;
        this.name = name;
        this.balance = 0.0;
        this.giftDistributions = new HashMap<>();
    }

    //Getters & Setters

    public Double getId() {
        return id;
    }

    public void setId(Double id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Map<Double, List<DefaultDeposit>> getGiftDistributions() {
        return giftDistributions;
    }

    public void setGiftDistributions(Map<Double, List<DefaultDeposit>> giftDistributions) {
        this.giftDistributions = giftDistributions;
    }
}
