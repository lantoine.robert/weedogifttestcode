package Java.Model.BusinessElements.Customer;

import Java.Model.BusinessElements.Deposits.DefaultDeposit;
import Java.Model.BusinessElements.Deposits.impl.DefaultGiftDeposit;

import java.util.ArrayList;
import java.util.List;

public class DefaultCustomer {
    private double id;
    private String name;
    private double balance;
    private List<DefaultDeposit> giftDistributions;

    ///Constructors
    public DefaultCustomer(double id, String name){
        this.id = id;
        this.name = name;
        this.balance = 0.0;
        this.giftDistributions = new ArrayList<>();
    }

    //Getters & Setters

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public List<DefaultDeposit> getGiftDistributions() {
        return giftDistributions;
    }

    public void setGiftDistributions(List<DefaultDeposit> giftDistributions) {
        this.giftDistributions = giftDistributions;
    }
}
