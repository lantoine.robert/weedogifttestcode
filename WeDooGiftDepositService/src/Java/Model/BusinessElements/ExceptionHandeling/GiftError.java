package Java.Model.BusinessElements.ExceptionHandeling;

public class GiftError extends Exception {

    public GiftError(){
        super();
    }

    public GiftError(String s){
        super(s);
    }
}
