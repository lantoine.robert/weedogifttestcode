package Java.Model.BusinessElements.ExceptionHandeling;

public class BalanceError extends Exception {

    public BalanceError(){ super(); }

    public BalanceError(String s){ super(s); }
}
