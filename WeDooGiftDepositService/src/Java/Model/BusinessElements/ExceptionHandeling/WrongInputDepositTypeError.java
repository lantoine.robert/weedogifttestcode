package Java.Model.BusinessElements.ExceptionHandeling;

public class WrongInputDepositTypeError extends Exception {

    public WrongInputDepositTypeError(){
        super();
    }

    public WrongInputDepositTypeError(String s){
        super(s);
    }
}
